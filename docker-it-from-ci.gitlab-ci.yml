# SPDX-FileCopyrightText: 2019 Florent Chauveau <florent.chauveau@gmail.com>
# SPDX-FileCopyrightText: 2019 Rigel Kent <sendmemail@rigelk.eu>
# SPDX-License-Identifier: GPL-3.0-or-later
# Comments: mentioned at https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/

### Docker build with push on tag, push on latest
##
#

# This is a GitLab CI configuration to build a project as a docker image
# REQUIREMENTS:
# - a Dockerfile in the project root
# - add `build` and `publish` stages if not already present to your .gitlab-ci.yml file and include the permanent raw url to this file, specifying the commit. Press `y` on the Gitlab page of this file to obtain it.
# - protect all your tags in the repository settings by creating a * wildcard
# - set the following variables:
#   - REGISTRY_IMAGE (full name of the docker image to be pulled/pushed)
#   - REGISTRY (full name of registry)
# - set the following protected variables:
#   - REGISTRY_USER
#   - REGISTRY_PASSWORD
# - (optionally) set the following variables:
#   - NO_PROXY
#   - HTTP_PROXY
#   - HTTPS_PROXY
#   - BRANCH
#   - DOCKERFILE (path to the Dockerfile)
#   - NO_DOCKER_PUSH_ON_COMMIT
#   - NO_DOCKER_PUSH_ON_LATEST
#   - NO_DOCKER_PUSH_ON_TAG
# - configure a runner with https://docs.gitlab.com/ce/ci/docker/using_docker_build.html#use-docker-in-docker-workflow-with-docker-executor
#   and tag it as 'docker'

stages:
  - build
  - publish

variables:
  BRANCH: master
  # When using dind service we need to instruct docker, to talk with the
  # daemon started inside of the service. The daemon is available with
  # a network connection instead of the default /var/run/docker.sock socket.
  #
  # The 'docker' hostname is the alias of the service container as described at
  # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
  #
  # Note that if you're using the Kubernetes executor, the variable should be set to
  # tcp://localhost:2375/ because of how the Kubernetes executor connects services
  # to the job container
  # DOCKER_HOST: tcp://localhost:2375/
  #
  # When using dind, it's wise to use the overlayfs driver for
  # improved performance.
  DOCKER_DRIVER: overlay2
  #
  # This will instruct Docker not to start over TLS.
  DOCKER_TLS_CERTDIR: ""
  # When TLS is disabled and for non-Kubernetes executors, we use tcp://docker:2375/
  DOCKER_HOST: tcp://docker:2375/
  #
  # This is the default registry (hub.docker.com)
  REGISTRY: https://index.docker.io/v1/
  BUILD_METHOD: 'kaniko'

.docker: &docker
  image: docker:19.03.1
  tags:
    # restrict to runners bearing the 'docker' tag. Make sure to tag
    # runners configured for DinD:
    # https://docs.gitlab.com/ce/ci/docker/using_docker_build.html#use-docker-in-docker-workflow-with-docker-executor
    - docker

.dockerlogin: &dockerlogin
  services:
    - docker:19.03.1-dind
  before_script:
    # docker login asks for the password to be passed through stdin for security
    # KEPT AS EXAMPLE IF USING GITLAB's OWN REGISTRY:
    # - docker login -u gitlab-ci-token --p $CI_JOB_TOKEN $REGISTRY
    - docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD $REGISTRY

build Dockerfile with DinD:
  <<: *docker
  <<: *dockerlogin
  stage: build
  only:
    variables:
      - $REGISTRY_IMAGE && $REGISTRY_USER && $REGISTRY_PASSWORD && $REGISTRY && $BUILD_METHOD == 'dind'
  script:
    # fetches the latest image (not failing if image is not found)
    - docker pull $REGISTRY_IMAGE:latest || true
    # builds the project, passing proxy variables, and vcs vars for LABEL
    # notice the cache-from, which is going to use the image we just pulled locally
    # the built image is tagged locally with the commit SHA, and then pushed to 
    # the GitLab registry
    - >
      docker build
      --pull
      --build-arg http_proxy=$http_proxy
      --build-arg https_proxy=$https_proxy
      --build-arg no_proxy=$no_proxy
      --build-arg VCS_REF=$CI_COMMIT_SHA
      --build-arg VCS_URL=$CI_PROJECT_URL
      --cache-from $REGISTRY_IMAGE:latest
      --tag $REGISTRY_IMAGE:$CI_COMMIT_SHA
      -f ${DOCKERFILE:-Dockerfile}
      .
    - ${NO_DOCKER_PUSH_ON_COMMIT:-false} || docker push $REGISTRY_IMAGE:$CI_COMMIT_SHA
  after_script:
    - docker images

build Dockerfile with Kaniko:
  <<: *docker
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  only:
    variables:
      - $REGISTRY_IMAGE && $REGISTRY_USER && $REGISTRY_PASSWORD && $REGISTRY && $BUILD_METHOD == 'kaniko'
  script:
    - echo "{\"auths\":{\"$REGISTRY\":{\"username\":\"$REGISTRY_USER\",\"password\":\"$REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - $NO_DOCKER_PUSH_ON_COMMIT == true && export PUSH="--no-push" || export PUSH=""
    # builds the project without root, passing proxy variables, and vcs vars 
    # for LABEL the built image is tagged locally with the commit SHA, and 
    # then pushed to the GitLab registry
    - >
      /kaniko/executor
      --cache=true
      --cache-repo $REGISTRY_IMAGE
      --context $CI_PROJECT_DIR
      --build-arg http_proxy=$http_proxy
      --build-arg https_proxy=$https_proxy
      --build-arg no_proxy=$no_proxy
      --build-arg VCS_REF=$CI_COMMIT_SHA
      --build-arg VCS_URL=$CI_PROJECT_URL
      $PUSH
      --dockerfile $CI_PROJECT_DIR/${DOCKERFILE:-Dockerfile}
      --destination $REGISTRY_IMAGE:$CI_COMMIT_SHA

publish Docker image as latest:
  <<: *docker
  <<: *dockerlogin
  variables:
    # We do not need GitLab to clone the source code.
    GIT_STRATEGY: none
  stage: publish
  only:
    variables:
      - $REGISTRY_IMAGE && $REGISTRY_USER && $REGISTRY_PASSWORD && $REGISTRY && $BRANCH == $CI_COMMIT_REF_NAME
  script:
    # Because we have no guarantee that this job will be picked up by the same runner 
    # that built the image in the previous step, we pull it again locally
    - docker pull $REGISTRY_IMAGE:$CI_COMMIT_SHA
    # Then we tag it "latest"
    - docker tag $REGISTRY_IMAGE:$CI_COMMIT_SHA $REGISTRY_IMAGE:latest
    # Annnd we push it.
    - docker push $REGISTRY_IMAGE:latest
  except:
    variables:
      - $NO_DOCKER_PUSH_ON_LATEST

# Finally, the goal here is to Docker tag any Git tag.
# GitLab will start a new pipeline everytime a Git tag is created.
publish Docker image as tag:
  <<: *docker
  <<: *dockerlogin
  variables:
    # Again, we do not need the source code here. Just playing with Docker.
    GIT_STRATEGY: none
  stage: publish
  only:
    refs:
      - tags
    variables:
      - $REGISTRY_IMAGE && $REGISTRY_USER && $REGISTRY_PASSWORD && $REGISTRY
  script:
    - docker pull $REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker tag $REGISTRY_IMAGE:$CI_COMMIT_SHA $REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    - docker push $REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  except:
    variables:
      - $NO_DOCKER_PUSH_ON_TAG
